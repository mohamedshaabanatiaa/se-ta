variable "AWS_SECRET_ACCESS_KEY" {
  type = string
  default = ""
}

variable "AWS_ACCESS_KEY_ID" {
  type = string
  default = ""
}

variable "Mongo_Password" {
  type = string
  default = ""
}

variable "Mongo_User" {
  type = string
  default = ""
}

variable "S3_AWS_ACCESS_KEY_ID" {
  type = string
  default = ""
}

variable "S3_AWS_SECRET_ACCESS_KEY" {
  type = string
  default = ""
}
