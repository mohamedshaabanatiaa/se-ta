provider "aws" {
  region = "eu-central-1"
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
}

##Existing VPC in eu-central-1
data "aws_vpc" "default-vpc" {
  id = "vpc-852046ef"
}

##Existing Public subnet in VPC in eu1-central-1
data "aws_subnet" "default-vpc-az1-subnet" {
  id = "subnet-b51d95f9"  
}

##EC2 Instance###################################################

# IAM Role
resource "aws_iam_role" "mongodb_iam_role" {
  name = "Database-MongoDB-IAM-Role-tf"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

# IAM Instance Profile
resource "aws_iam_instance_profile" "mongodb_instance_profile" {
  name = "Database-MongoDB-IAM-Role-tf"
  role = aws_iam_role.mongodb_iam_role.name
}

# Attach S3 AWS Managed Policy to IAM Role
resource "aws_iam_role_policy_attachment" "s3_full_access" {
  role       = aws_iam_role.mongodb_iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

# Customer Managed Policy for EC2 permissions
resource "aws_iam_policy" "mongodb_ec2_policy" {
  name        = "Database-MongoDB-Instance-Profile-Policy-tf"
  description = "Customer managed policy to allow all EC2 actions"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "ec2:*"
        Effect = "Allow"
        Resource = "*"
      }
    ]
  })
}

# Attach Customer Managed Policy to IAM Role
resource "aws_iam_role_policy_attachment" "ec2_policy_attachment" {
  role       = aws_iam_role.mongodb_iam_role.name
  policy_arn = aws_iam_policy.mongodb_ec2_policy.arn
}


# Security Group
resource "aws_security_group" "Database-MongoDB-tf" {
  vpc_id = "vpc-852046ef"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = ["172.31.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Database-MongoDB-tf"
  }
}


#Ec2 Instance
resource "aws_instance" "Database-MongoDB-tf" {
  ami           = "ami-0578f46b79ca9e3e7"
  instance_type = "t2.micro"
  key_name      = "mb-eu-central-1"
  subnet_id     = "subnet-b51d95f9"
  vpc_security_group_ids = [aws_security_group.Database-MongoDB-tf.id ]
  iam_instance_profile  = aws_iam_instance_profile.mongodb_instance_profile.name

  tags = {
    Name = "Database-MongoDB-tf"
  }

  user_data = <<-EOF
              #!/bin/bash
              # Update the system
              sudo yum update -y
              
              # Install EPEL repository
              sudo amazon-linux-extras install epel -y
              
              # Install MongoDB
              sudo tee -a /etc/yum.repos.d/mongodb-org-7.0.repo <<EOL
              [mongodb-org-7.0]
              name=MongoDB Repository
              baseurl=https://repo.mongodb.org/yum/amazon/2/mongodb-org/7.0/x86_64/
              gpgcheck=1
              enabled=1
              gpgkey=https://pgp.mongodb.com/server-7.0.asc 
              EOL
              sudo yum install -y mongodb-org
              sudo systemctl start mongod
              sudo systemctl enable mongod

              # Configure MongoDB to bind to all IPs
              sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mongod.conf
              sudo systemctl restart mongod

              # Create MongoDB user
              sleep 20
              mongosh admin --eval 'db.createUser({user: "${var.Mongo_User}", pwd: "${var.Mongo_Password}", roles: ["root"]})'

              # Install s3cmd
              sudo yum --enablerepo epel install s3cmd -y
              sudo bash -c 'echo -e "[default]\naccess_key =${var.S3_AWS_ACCESS_KEY_ID}\nsecret_key =${var.S3_AWS_SECRET_ACCESS_KEY}" > /home/ec2-user/.s3cfg'
              sudo bash -c 'echo -e "[default]\naccess_key =${var.S3_AWS_ACCESS_KEY_ID}\nsecret_key =${var.S3_AWS_SECRET_ACCESS_KEY}" > /root/.s3cfg'
              # Create cron job for MongoDB backup
              sudo tee -a backup.sh > /home/ec2-user/backup.sh << 'EOL'
              #!/bin/bash
              mkdir mongodump
              mongosh admin --eval "printjson(db.fsyncLock())" -u "${var.Mongo_User}" -p "${var.Mongo_Password}"
              TIMESTAMP=`date +%F-%H%M`
              S3_BUCKET_NAME="database-mongodb-backup-tf"
              S3_BUCKET_PATH="mongodb-backups"
              MONGODUMP_PATH="/usr/bin/mongodump"
              MONGO_DATABASE="go-mongodb"
              $MONGODUMP_PATH -d $MONGO_DATABASE -u "${var.Mongo_User}" -p "${var.Mongo_Password}" --authenticationDatabase admin --out /home/ec2-user/mongodump
              mv /home/ec2-user/mongodump mongodb-$HOSTNAME-$TIMESTAMP
              tar cf mongodb-$HOSTNAME-$TIMESTAMP.tar mongodb-$HOSTNAME-$TIMESTAMP
              s3cmd put mongodb-$HOSTNAME-$TIMESTAMP.tar s3://$S3_BUCKET_NAME/$S3_BUCKET_PATH/mongodb-$HOSTNAME-$TIMESTAMP.tar
              mongosh admin --eval "printjson(db.fsyncUnlock())" -u "${var.Mongo_User}" -p "${var.Mongo_Password}"
              rm -rf mongodump
              rm -rf mongodb-*
              EOL
              sudo sudo bash -c 'echo "00 09 * * * ec2-user /bin/bash /home/ec2-user/backup.sh" > /etc/cron.d/mongodb_backup'
              /bin/bash /home/ec2-user/backup.sh
              EOF
}


#S3 Bucket##################################################

resource "aws_s3_bucket" "mongodb_backup_bucket" {
  bucket = "database-mongodb-backup-tf"
  acl    = "private"  # Disabling ACL
  force_destroy = true
}

##Enable Public Access
resource "aws_s3_bucket_public_access_block" "mongodb_backup_bucket" {
  bucket = aws_s3_bucket.mongodb_backup_bucket.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}


##EKS Cluster###############################################

resource "aws_eip" "nat_gateway_eip" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_gateway_eip.id
  subnet_id     = "subnet-b51d95f9"
}

resource "aws_route_table" "private_rt" {
  vpc_id = data.aws_vpc.default-vpc.id

  route {
    cidr_block = "172.31.0.0/16"
    gateway_id = "local"
  }

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway.id
  }
}

resource "aws_subnet" "eks_private_subnet_1" {
  vpc_id            = data.aws_vpc.default-vpc.id
  cidr_block        = "172.31.64.0/22"
  availability_zone = "eu-central-1a"
}

resource "aws_subnet" "eks_private_subnet_2" {
  vpc_id            = data.aws_vpc.default-vpc.id
  cidr_block        = "172.31.68.0/22"
  availability_zone = "eu-central-1b"
}

resource "aws_route_table_association" "private_subnet_1_assoc" {
  subnet_id      = aws_subnet.eks_private_subnet_1.id
  route_table_id = aws_route_table.private_rt.id
}

resource "aws_route_table_association" "private_subnet_2_assoc" {
  subnet_id      = aws_subnet.eks_private_subnet_2.id
  route_table_id = aws_route_table.private_rt.id
}

resource "aws_security_group" "eks_cluster_sg" {
  name_prefix = "shaaban-eks-cluster-sg"
  description = "EKS cluster security group"
  vpc_id      = data.aws_vpc.default-vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_iam_role" "eks_cluster_role" {
  name = "eks_cluster_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Effect = "Allow"
      Principal = {
        Service = "eks.amazonaws.com"
      }
      Action = "sts:AssumeRole"
    }]
  })
}

resource "aws_iam_role_policy_attachment" "eks_cluster_policy" {
  role       = aws_iam_role.eks_cluster_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

data "aws_iam_role" "existing_node_role" {
  name = "AmazonEKSNodeRole"
}

resource "aws_eks_cluster" "shaaban_eks_cluster" {
  name     = "Shaaban-EKs-Cluster"
  role_arn = aws_iam_role.eks_cluster_role.arn
  version  = "1.29"

  vpc_config {
    subnet_ids = [
      aws_subnet.eks_private_subnet_1.id,
      aws_subnet.eks_private_subnet_2.id
    ]
    security_group_ids = [aws_security_group.eks_cluster_sg.id]
  }
}

resource "aws_eks_node_group" "shaaban_ng" {
  cluster_name    = aws_eks_cluster.shaaban_eks_cluster.name
  node_group_name = "shaaban-ng"
  node_role_arn   = data.aws_iam_role.existing_node_role.arn
  subnet_ids      = [
    aws_subnet.eks_private_subnet_1.id,
    aws_subnet.eks_private_subnet_2.id
  ]
  
  scaling_config {
    desired_size = 1
    max_size     = 1
    min_size     = 1
  }

  instance_types = ["t3.medium"]
  ami_type       = "AL2_x86_64"
  capacity_type  = "ON_DEMAND"
}
